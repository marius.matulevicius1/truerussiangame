﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Play : MonoBehaviour {
    public MovieTexture movie;
    public AudioSource sound;
    // Use this for initialization
    void Start()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        sound.clip = movie.audioClip;
        movie.Play();
        sound.Play();
    }

}
