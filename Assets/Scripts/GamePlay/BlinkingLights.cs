﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkingLights : MonoBehaviour {

    public GameObject Lights;
    public float timer = .3f;
    bool t = true;
    // Update is called once per frame

    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(timer);
            Lights.SetActive(t);
            t = !t;

        }

    }

    
}
