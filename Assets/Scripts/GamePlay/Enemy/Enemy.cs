﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {

    public float health = 100f;
    public GameObject Player;
    Animator anim;
    public float speed;
    public float distance;
    bool isDead = false;
    Rigidbody2D rgb;
    float timer = 0;
    public bool canMove = true;
	// Use this for initialization
	void Start () {
        rgb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        Physics2D.IgnoreCollision(Player.GetComponent<CapsuleCollider2D>(), GetComponent<CapsuleCollider2D>());
        Physics2D.IgnoreLayerCollision(8, 8);
	}
	
	// Update is called once per frame
	void Update () {
        distance = Vector2.Distance(transform.position, Player.transform.position);
        Rotate();
        MoveToPlayer();
        if(distance <15 && distance > 10)
        {
        }
    }


    void MoveToPlayer()
    {
        if (gameObject.name == "Boss" && distance > 20) return;
        if (distance > 1.5f && !isDead && canMove)
        {
            if (Player.transform.position.x < transform.position.x)
            {
                rgb.velocity = new Vector2(-speed, rgb.velocity.y);
            }
            if (Player.transform.position.x > transform.position.x)
            {
                rgb.velocity = new Vector2(speed, rgb.velocity.y);
            }

        }
    }

    void Attack()
    {
        if(timer > 4f)
        if(distance <= 1.6)
        {
            timer = 0f;
            anim.SetBool("Attack", true);
        }
    }

    void Rotate()
    {
        if (Player.transform.position.x < transform.position.x)
        {
            if (transform.localScale.x > 0)
            {
                Vector3 Scale = transform.localScale;
                Scale.x *= -1;
                transform.localScale = Scale;
            }
        }
        if (Player.transform.position.x > transform.position.x)
        {
            if (transform.localScale.x < 0)
            {
                Vector3 Scale = transform.localScale;
                Scale.x *= -1;
                transform.localScale = Scale;
            }
            }
        }

    void Dead()
    {
        canMove = false;
        anim.SetBool("Dead", true);
        Destroy(gameObject, 5f);
        
        Destroy(this);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (Player.transform.position.x < transform.position.x)
                rgb.velocity = new Vector2(7, 0);
            else
                rgb.velocity = new Vector2(-7, 0);
            Dead();
            /* anim.SetBool("GotHit", true);
            health -= 10;
            if(Player.transform.position.x < transform.position.x)
            rgb.velocity = new Vector2(2, 0);
            else
            rgb.velocity = new Vector2(-2, 0);
            if (health <= 0)
                Dead();
                */
        }
    }

}
