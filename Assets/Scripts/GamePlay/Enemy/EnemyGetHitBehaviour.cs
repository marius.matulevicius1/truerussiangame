﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGetHitBehaviour : StateMachineBehaviour {


    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("GotHit", false);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {

    }

}
