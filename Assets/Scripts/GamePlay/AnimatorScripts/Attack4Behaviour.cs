﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack4Behaviour : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<PlayerController>().canMove = false;
        animator.gameObject.GetComponent<PlayerController>().meleeHitbox.enabled = true;
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("Attack4", false);
    }
    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<PlayerController>().canMove = true;
        animator.gameObject.GetComponent<PlayerController>().meleeHitbox.enabled = false;
    }
}
