﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour {
    AudioSource audi;
    public AudioClip yeee;
    public AudioClip OpOp;
    public Slider DrunknessSlider;
    float currentDrunkness = 30f;
    Collider2D coll;
    public bool canMove = true;
    bool canDrink = false;
    Rigidbody2D rgb;
    Animator anim;
    public float moveSpeed;
    bool facingRight = true;
    public BoxCollider2D meleeHitbox;
	// Use this for initialization
	void Start () {
        audi = GetComponent<AudioSource>();
        rgb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        meleeHitbox.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentDrunkness > 100) currentDrunkness = 100;
        DrunknessSlider.value = Mathf.Lerp(DrunknessSlider.value, currentDrunkness, 0.1f);
        if (Input.GetKeyDown(KeyCode.E) && canDrink)
        {
            Destroy(coll.gameObject);
            anim.SetBool("Drink", true);
            currentDrunkness += 20;
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            anim.SetBool("Drink", false);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            SceneManager.LoadScene("FinalBossPunch");
        }

        Movement();
        Attack();

    }

    public void Movement()
    {
        if (!canMove)
            return;

        if (Input.GetKey(KeyCode.A))
        {
            if (transform.localScale.x > 0) Flip();
            rgb.velocity = new Vector2(-moveSpeed, rgb.velocity.y);
            anim.SetFloat("Speed", Mathf.Abs(rgb.velocity.x));
        }
        else
       if (Input.GetKey(KeyCode.D))
        {
            if (transform.localScale.x < 0) Flip();
            rgb.velocity = new Vector2(moveSpeed, rgb.velocity.y);
            anim.SetFloat("Speed", Mathf.Abs(rgb.velocity.x));
        }
        else
        {
            rgb.velocity = new Vector2(0f, rgb.velocity.y);
            anim.SetFloat("Speed", Mathf.Abs(rgb.velocity.x));
        }
    }



    void Flip()
    {
        Vector3 Scale = transform.localScale;
        Scale.x *= -1;
        transform.localScale = Scale;
        facingRight = !facingRight;

    }

    void Attack()
    {
        if (Input.GetKey(KeyCode.Y))
        {
            if(!audi.isPlaying)
            audi.PlayOneShot(OpOp);
            anim.SetBool("Attack1", true);
             if (facingRight)
                 rgb.velocity = new Vector3(2, rgb.velocity.y);
             else
                 rgb.velocity = new Vector3(-2, rgb.velocity.y);
                 
        }


        if (Input.GetKey(KeyCode.U))
        {
            anim.SetBool("Attack2", true);
            rgb.velocity = new Vector3(0, 0);
            /* if (facingRight)
                 rgb.velocity = new Vector3(2, rgb.velocity.y);
             else
                 rgb.velocity = new Vector3(-2, rgb.velocity.y);
                 */
        }


        if (Input.GetKey(KeyCode.I))
        {
            anim.SetBool("Attack3", true);
            if (!audi.isPlaying)
                audi.PlayOneShot(yeee);
            if (facingRight)
                rgb.velocity = new Vector3(4, rgb.velocity.y);
            else
                rgb.velocity = new Vector3(-4, rgb.velocity.y);
        }


    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        coll = collision;
        if (collision.gameObject.tag == "Collectables")
        {
            canDrink = true;
        }

        if(collision.gameObject.tag == "Table")
        {
            moveSpeed += 2;
            anim.SetBool("SlideTable", true);
            meleeHitbox.enabled = true;
        }

        if (collision.gameObject.tag == "WetFloor") 
        {
            anim.SetBool("Slip", true);
            canMove = false;
            if (facingRight)
                rgb.velocity = new Vector3(4, rgb.velocity.y);
            else
                rgb.velocity = new Vector3(-4, rgb.velocity.y);
        }

        if(collision.gameObject.name == "Boss")
        {
            SceneManager.LoadScene("FinalBossPunch");
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collectables")
        {
            canDrink = false;
        }

        if (collision.gameObject.tag == "Table")
        {
            anim.SetBool("SlideTable", false);
            moveSpeed -= 2;
            meleeHitbox.enabled = false;
        }
        if (collision.gameObject.tag == "WetFloor")
        {
            canMove = true;
            anim.SetBool("Slip", false);
        }

    }




}
