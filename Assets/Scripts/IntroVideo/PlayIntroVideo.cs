﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayIntroVideo : MonoBehaviour {
    public MovieTexture movie;
    public AudioSource sound;
    // Use this for initialization
    void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        sound.clip = movie.audioClip;
        movie.Play();
        sound.Play();

        StartCoroutine("wait");
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(movie.duration);
        
        SceneManager.LoadScene("GamePlay");
    }
}
